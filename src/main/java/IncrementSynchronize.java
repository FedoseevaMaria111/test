import java.io.File;
import java.util.concurrent.locks.ReentrantLock;

public class IncrementSynchronize {
    private int value = 0;
    private final Object lock = new Object();
    private final ReentrantLock reentrantLock = new ReentrantLock();

    public synchronized void increment() {
        value++;
    }
    public synchronized int increment1() {
        return value;
    }

    public void getNextValueSynchronized() {
        synchronized (lock) {
            value++;
        }
    }

    public void getNextValueReentrantLock() {
        reentrantLock.lock();
        try {
            value++;
        } finally {
            reentrantLock.unlock();
        }
    }
    public int getValue() {
        return value;
    }
}

