import java.util.*;

public class GenericMethod {
    public  <T> Collection<T> fromArrayToCollection(T[] a, Collection<T> c) {
        {
            Objects.requireNonNull(a);
            Objects.requireNonNull(c);
            Collections.addAll(c, a);
            return c;
        }

    }
}