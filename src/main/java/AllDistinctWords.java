/*
  Write a program to find all distinct words from a text file.
  Ignore chars like ".,/-;:" and Ignore case sensitivity.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

public class AllDistinctWords {
    public static void main(String[] args) {
        try {
            FileReader in = new FileReader("C:\\Users\\MASHA\\IdeaProjects\\practice\\src\\main\\java\\Entry_test\\sample.txt");
            BufferedReader br = new BufferedReader(in);
            Set<String> DistinctWords = new HashSet<String>();
            String[] words;
            String line;
            while ((line = br.readLine()) != null) {
                words = line.split("[-!~\\s]+");
                for (String word : words) {
                    DistinctWords.add(word);
                }

            }

            System.out.println(DistinctWords);
        } catch (Exception e) {
        }
    }
        }




